<i>PeerTube Live</i> vous permet de diffuser la caméra de votre smartphone vers votre chaîne PeerTube.

Connectez vous à tout le Fédiverse en direct depuis votre appareil.
<i>(Nécessite un compte sur un hébergement PeerTube)</i>

<br><b>Fonctionnalités :</b>

* Fonctionne avec n'importe quelle instance PeerTube !
* Prise en charge de multiples comptes
* Paramétrez votre live stream comme vous le voulez (description, titre, visibilité…)
* Changement de caméra (avant/arrière) en un clic
* Facile à utiliser

Développé par l'association <a href="https://framasoft.org/" rel="nofollow" target="_blank">Framasoft</a>, PeerTube est une alternative libre et fédérée aux plateformes de vidéo centralisatrices. Découvrez PeerTube sur <a href="https://joinpeertube.org/" rel="nofollow" target="_blank">joinpeertube.org</a> !
